package com.arkumbra.examplegame.drawable;

import android.graphics.Canvas;

/**
 * Created by lukegardener on 2017/07/28.
 */

public interface GameDrawable {

    void draw(Canvas canvas);
}
